# voonyxchallenge-net

Laboratoire Drone
Prototype pour contrôler les drônes

Ce petit défi technique est assez simple, mais permet de valider les connaissances et la compréhension générale d'un projet. Le projet est fonctionnel à la base, mais pourrait bénéficier d'améliorations.

Le projet est de type web, et permet d'afficher les différents drones du laboratoire ainsi que ses spécifications.
Vous pouvez ajouter des packages pour compléter les points

Pour ce projet, nous vous demandons les choses suivantes:

    - Extraire _baseUrl du controlleur Specification et ajouter dans le web.config, puis le lire
    - Créer un fichier web.config pour la production avec le transform http://app.voonyx.ca/api/specifications
    - Créer une méthode d'extension au modèle SpecificationModel, qui retourne le nombre de jour entre la date d'aujourd'hui et la date publié du modèle «PublishedDate» 
	- Utiliser votre méthode d'extension dans la view Detail.cshtml
    - Intégrer l'injection de dépendance
    - Documenter le service web pour nos fournisseurs externes
    - Remplacer les assignations de propriétés répétitives entre les différents entités ( modèle )
    - Compléter les tests du service web ( utiliser framework ou package voulu )
    - Intégrer EF dans la couche d'accès données pour récupérer les données de la bd (Voonyx.Drone.Data.Db)

Une fois le défi complété, vous pouvez soumettre votre projet en fichier .zip à l'adresse: http://voonyx.ca/fr/defi