﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using Voonyx.Drone.Data;

namespace Voonyx.Drone.Services.Controllers
{
    public class SpecificationsController : ApiController
    {       
        [HttpGet]
        [ResponseType(typeof(Models.Specification))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var specAccess = new SpecificationsAccess();
                var makeAccess = new MakesAccess();
                var modelAccess = new ModelsAccess();
                var specification = specAccess.Get(id);

                var specModel = new Models.Specification()
                {
                    Id = specification.Id,
                    Make = makeAccess.Get(specification.MakeId).Description,
                    Model = modelAccess.Get(specification.ModelId).Description,
                    Weight = specification.Weight,
                    MaxSpeed = specification.MaxSpeed,
                    Price = specification.Price,
                    Published = specification.Published
                };

                return Ok(specModel);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }                      
        }

        [HttpGet]
        [ResponseType(typeof(List<Models.Specification>))]
        public IHttpActionResult Get()
        {
            try
            {
                var specAccess = new SpecificationsAccess();
                var makeAccess = new MakesAccess();
                var modelAccess = new ModelsAccess();
                var allSpecs = specAccess.GetAll();

                var specsItems = new List<Models.Specification>();

                foreach (var item in allSpecs)
                {
                    var spec = new Models.Specification()
                    {
                        Id = item.Id,
                        Make = makeAccess.Get(item.MakeId).Description,
                        Model = modelAccess.Get(item.ModelId).Description,
                        Weight = item.Weight,
                        MaxSpeed = item.MaxSpeed,
                        Price = item.Price,
                        Published = item.Published
                    };
                    specsItems.Add(spec);
                }

                return Ok(specsItems);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
