﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using Voonyx.Drone.Web.Models;
using Voonyx.Drone.Web.Models.Services;
using Voonyx.Drone.Web.Services;

namespace Voonyx.Drone.Web.Controllers
{
    public class SpecificationController : Controller
    {
        private const string _baseUrl = "http://localhost:48180/api/specifications";
        public async Task<ActionResult> All()
        {
            var service = new RestClientService();
            var serviceSpecs = await service
                .WithPath(_baseUrl)
                .Get<List<Specification>>();

            var allSpecs = new List<SpecificationModel>();

            foreach (var item in serviceSpecs)
            {
                var model = new SpecificationModel()
                {
                    Id = item.Id,
                    Make = item.Make,
                    Model = item.Model,
                    Weight = item.Weight,
                    MaxSpeed = item.MaxSpeed,
                    CustomerPrice = item.Price,
                    PublishedDate = item.Published
                };
                allSpecs.Add(model);
            }

            return View(allSpecs);
        }

        public async Task<ActionResult> Detail(int Id)
        {
            var service = new RestClientService();
            var serviceSpec = await service
                .WithPath($"{_baseUrl}/{Id}")
                .Get<Specification>();
        
            var model = new SpecificationModel()
            {
                Id = serviceSpec.Id,
                Make = serviceSpec.Make,
                Model = serviceSpec.Model,
                Weight = serviceSpec.Weight,
                MaxSpeed = serviceSpec.MaxSpeed,
                CustomerPrice = serviceSpec.Price, 
                PublishedDate = serviceSpec.Published
            };
            
            return View(model);
        }
    }
}