﻿using System;

namespace Voonyx.Drone.Web.Models
{
    public class SpecificationModel
    {
        public int Id { get; set; }
        public string Model { get; set; }
        public string Make { get; set; }
        public decimal Weight { get; set; }
        public decimal MaxSpeed { get; set; }
        public decimal CustomerPrice { get; set; }
        public DateTime PublishedDate { get; set; }
        public string FullDescription { get { return $"{Model}-{Make}"; } }
    }
}