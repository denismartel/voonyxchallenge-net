﻿using System;

namespace Voonyx.Drone.Web.Models.Services
{
    public class Specification
    {
        public int Id { get; set; }
        public string Model { get; set; }
        public string Make { get; set; }
        public decimal Weight { get; set; }
        public decimal MaxSpeed { get; set; }

        public decimal Price { get; set; }
        public DateTime Published { get; set; }

    }
}