﻿using System.Threading.Tasks;

namespace Voonyx.Drone.Web.Services
{
    public interface IRestClientService
    {
        Task<T> Get<T>();
    }
}
