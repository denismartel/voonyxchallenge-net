﻿using System.Threading.Tasks;
using DalSoft.RestClient;

namespace Voonyx.Drone.Web.Services
{
    public class RestClientService : IRestClientService
    {
        private string _path = "";

        public RestClientService WithPath(string path)
        {
            _path = path;
            return this;
        }
        public async Task<T> Get<T>()
        {
            dynamic client = new RestClient(_path, new Headers { });
            
            var response = await client.Get();

            return response;
        }
    }
}
