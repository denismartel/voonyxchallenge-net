﻿using System.Collections.Generic;

namespace Voonyx.Drone.Data
{
    public interface IDataAccess<T>
    {
        IList<T> GetAll();
        
        T Get(int Id);       
    }
}
