﻿using System.Collections.Generic;
using System.Linq;
using Voonyx.Drone.Data.Models;

namespace Voonyx.Drone.Data
{
    public class SpecificationsAccess : IDataAccess<Specification>
    {

        public IList<Specification> _specifications = new List<Specification>()
        {
            new Specification() { Id = 1, MakeId = 1, ModelId = 1, MaxSpeed = 20, Weight = 1380, Height = 12, Width = 23, Price = 2234.75m, Published = new System.DateTime(2018,5,12) } ,
            new Specification() { Id = 2, MakeId = 1, ModelId = 2, MaxSpeed = 18, Weight = 1050, Height = 11.2m, Width = 20.5m, Price = 1950.75m, Published = new System.DateTime(2018,7,21) } ,
            new Specification() { Id = 3, MakeId = 1, ModelId = 3, MaxSpeed = 23, Weight = 1215, Height = 8.76m, Width = 25.63m, Price = 2843.62m, Published = new System.DateTime(2018,11,22)  }
        };

        public IList<Specification> GetAll()
        {
            return _specifications;
        }

        public Specification Get(int Id)
        {
            return _specifications.Where(x => x.Id == Id).FirstOrDefault();
        }

    }
}
