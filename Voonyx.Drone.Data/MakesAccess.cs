﻿using System.Collections.Generic;
using System.Linq;
using Voonyx.Drone.Data.Models;

namespace Voonyx.Drone.Data
{
    public class MakesAccess : IDataAccess<Make>
    {

        public IList<Make> _makes = new List<Make>()
        {
            new Make() { Id = 1, Description = "DJI"  }             
        };

        public IList<Make> GetAll()
        {
            return _makes;
        }

        public Make Get(int Id)
        {
            return _makes.Where(x => x.Id == Id).FirstOrDefault();
        }

    }
}
