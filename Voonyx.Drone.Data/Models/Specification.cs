﻿using System;

namespace Voonyx.Drone.Data.Models
{
    public class Specification
    {
        public int Id { get; set; }
        public int ModelId { get; set; }
        public int MakeId { get; set; }
        public decimal Weight { get; set; }
        public decimal MaxSpeed { get; set; }
        public decimal Height { get; set; }
        public decimal Width { get; set; }
        public decimal Price { get; set; }
        public DateTime Published { get; set; }
    }
}