﻿using System.Collections.Generic;
using System.Linq;
using Voonyx.Drone.Data.Models;

namespace Voonyx.Drone.Data
{
    public class ModelsAccess : IDataAccess<Model>
    {

        public IList<Model> _models = new List<Model>()
        {
            new Model() { Id = 1, Description = "Phantom 4"  },
            new Model() { Id = 2, Description = "Mavic 2"  },
            new Model() { Id = 3, Description = "Mavic Air Pro 2"  }
        };

        public IList<Model> GetAll()
        {
            return _models;
        }

        public Model Get(int Id)
        {
            return _models.Where(x => x.Id == Id).FirstOrDefault();
        }

    }
}
